<?php
require_once 'model/bank.php';

class BankController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new Bank();
    }
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/bank/bank.php';
        require_once 'view/footer.php';
    }
    
    public function Crud(){
        $alm = new Bank();
        
        if(isset($_REQUEST['id'])){
            $alm = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'view/header.php';
        require_once 'view/bank/bank-editar.php';
        require_once 'view/footer.php';
    }
    
    public function Guardar(){
        $alm = new Bank();
        
        $alm->id = $_REQUEST['id'];
        $alm->name = $_REQUEST['name'];
        $alm->code = $_REQUEST['code'];
        $alm->country = $_REQUEST['country'];

        $alm->id > 0 
            ? $this->model->Actualizar($alm)
            : $this->model->Registrar($alm);
        
        header('Location: index.php?c=Bank');
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php?c=Bank');
    }
}