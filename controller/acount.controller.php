<?php
require_once 'model/acount.php';
require_once 'model/bank.php';
require_once 'model/client.php';

class AcountController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new Acount();
        $this->bancos = new Bank();
        $this->clientes = new Client();
    }
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/acount/Acount.php';
        require_once 'view/footer.php';
    }
    
    public function Crud(){
        $alm = new Acount();
        
        if(isset($_REQUEST['id'])){
            $alm = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'view/header.php';
        require_once 'view/acount/acount-editar.php';
        require_once 'view/footer.php';
    }
    
    public function Guardar(){
        $alm = new Acount();
        
        $alm->id = $_REQUEST['id'];
        $alm->bank_id = $_REQUEST['bank_id'];
        $alm->client_id = $_REQUEST['client_id'];
        $alm->code = $_REQUEST['code'];

        $alm->id > 0 
            ? $this->model->Actualizar($alm)
            : $this->model->Registrar($alm);
        header('Location: index.php?c=Acount');
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php?c=Acount');
    }
}