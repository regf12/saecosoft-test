<?php
require_once 'database.php';
header('Content-type:application/xls');
header('Content-Disposition: attachment; filename=reporte.xls');
$link = Database::connect();
$html='';

$cliente = array();
$cuenta = array();
$banco = array();

$bancos = $link->query("SELECT * FROM banks");
while ($row = $bancos->fetch(\PDO::FETCH_ASSOC)) {
    $banco[]=$row;
}
$clientes = $link->query("SELECT * FROM clients");
while ($row = $clientes->fetch(\PDO::FETCH_ASSOC)) {
    $cliente[]=$row;
}
$cuentas = $link->query("SELECT * FROM acounts");
while ($row = $cuentas->fetch(\PDO::FETCH_ASSOC)) {
    $cuenta[]=$row;
}

foreach($banco as $b):
    $cont=0;
    $html = $html.'<b>Banco '.$b['name'].'</b><br>';
    $html = $html.'<table border=1>';
    $html = $html.'<thead><tr><th>#</th><th>Nombre</th><th>Correo</th><th>cuenta</th></tr></thead><tbody>';
    foreach($cliente as $c):
        foreach($cuenta as $a):
            if($c['id']==$a['client_id'] && $b['id']==$a['bank_id']){
                $cont++;
                $html = $html.'<tr><td>'.$cont.'</td>';
                $html = $html.'<td>'.$c['name'].'</td>';
                $html = $html.'<td>'.$c['email'].'</td>';
                $html = $html.'<td>'.$a['code'].'</td></tr>';
            }
        endforeach;
    endforeach;
    $html = $html.'</tbody></table>';
endforeach;

echo $html;
