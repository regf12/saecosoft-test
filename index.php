<?php
# repositorio:
# https://gitlab.com/regf12/saecosoft-test.git

require_once 'config/database.php';

/*try {
    Database::get()->connect();
    echo 'Conexion exitosa.';
} catch (\PDOException $e) {
    echo $e->getMessage();
}*/

$controller = 'client';

if(!isset($_REQUEST['c']))
{
    require_once "controller/$controller.controller.php";
    $controller = ucwords($controller) . 'Controller';
    $controller = new $controller;
    $controller->Index();
}
else
{
    $controller = strtolower($_REQUEST['c']);
    $accion = isset($_REQUEST['a']) ? $_REQUEST['a'] : 'Index';
    
    require_once "controller/$controller.controller.php";
    $controller = ucwords($controller) . 'Controller';
    $controller = new $controller;
    
    call_user_func( array( $controller, $accion ) );
}