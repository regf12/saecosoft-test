<!DOCTYPE html>
<html lang="es">
	<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Test</title>

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <script src="assets/js/jquery-1.11.3.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/main.js"></script>
        
	</head>
    <body>
        
    <nav class="navbar navbar-expand-sm navbar-light bg-light">
      <div class="navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="index.php?c=Client">Clientes <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?c=Bank">Bancos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?c=Acount">Cuentas</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reportes</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="config/pdf.php">Pdf</a>
              <a class="dropdown-item" href="config/excel.php">Ecxel</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">