<h1 class="page-header">Bancos</h1>

<div class="well well-sm text-right">
    <a class="btn btn-primary" href="?c=Bank&a=Crud">Nuevo banco</a>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            <th style="width:180px;">Nombre</th>
            <th>Codigo</th>
            <th>Pais</th>
            <th style="width:60px;"></th>
            <th style="width:60px;"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
            <td><?php echo $r['name']; ?></td>
            <td><?php echo $r['code']; ?></td>
            <td><?php echo $r['country']; ?></td>
            <td>
                <a href="?c=Bank&a=Crud&id=<?php echo $r['id']; ?>">Editar</a>
            </td>
            <td>
                <a onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=Bank&a=Eliminar&id=<?php echo $r['id']; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 
