<h1 class="page-header">
    <?php echo $alm->id != null ? $alm->name : 'Nuevo Registro'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=Bank">Bancos</a></li>
  <li class="active"><?php echo $alm->id != null ? $alm->name : 'Nuevo Registro'; ?></li>
</ol>

<form id="frm-submit" action="?c=Bank&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $alm->id; ?>" />
    
    <div class="form-group">
        <label>Nombre</label>
        <input type="text" name="name" value="<?php echo $alm->name; ?>" class="form-control" placeholder="Ingrese el nombre del banco" data-validacion-tipo="requerido|min:3" />
    </div>

    <div class="form-group">
        <label>Codigo</label>
        <input type="number" name="code" value="<?php echo $alm->code; ?>" class="form-control" placeholder="Ingrese el codigo del banco" data-validacion-tipo="requerido|min:4" />
    </div>
    
    <div class="form-group">
        <label>Pais</label>
        <input type="text" name="country" value="<?php echo $alm->country; ?>" class="form-control" placeholder="Indique donde se encuentra al banco" data-validacion-tipo="requerido|min:3" />
    </div>
    
    <hr />
    
    <div class="text-right">
        <button class="btn btn-success">Guardar</button>
    </div>
</form>
