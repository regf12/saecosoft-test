<h1 class="page-header">
</h1>

<ol class="breadcrumb">
  <li><a href="?c=Acount">Cuentas</a></li> 
</ol>

<form id="frm-submit" action="?c=Acount&a=Guardar" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $alm->id; ?>" />
    
    <div class="form-group">
      <label>Cliente</label>
      <select name="client_id" value="<?php echo $alm->client_id; ?>" class="form-control" id="cliente" data-validacion-tipo="requerido">
        <option value="null" >Seleccione</option>
        <?php foreach($this->clientes->Listar() as $c):  ?>
          <option value="<?php echo $c['id']; ?>" ><?php echo $c['name']; ?></option> 
        <?php endforeach; ?>
      </select>
    </div>

    <div class="form-group">
      <label>Banco</label>
      <select name="bank_id" value="<?php echo $alm->bank_id; ?>" class="form-control" id="banco" data-validacion-tipo="requerido">
        <option value="null" >Seleccione</option>
        <?php foreach($this->bancos->Listar() as $b):  ?>
          <option value="<?php echo $b['id']; ?>" ><?php echo $b['name']; ?></option> 
        <?php endforeach; ?>
      </select>
    </div>

    <div class="form-group">
      <label>Codigo de cuenta</label>
      <input type="number" name="code" value="<?php echo$alm->code; ?>" class="form-control" placeholder="Ingrese su numero de cuenta bancaria" data-validacion-tipo="requerido" />
    </div>
    
    <hr />
    
    <div class="text-right">
        <button class="btn btn-success">Guardar</button>
    </div>
</form>

<script>
$(function(){
    $("#cliente").val(<?php echo $alm->client_id; ?>);
    $("#banco").val(<?php echo $alm->bank_id; ?>);
});
</script>