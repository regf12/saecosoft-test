<h1 class="page-header">Cuentas</h1>

<div class="well well-sm text-right">
    <a class="btn btn-primary" href="?c=Acount&a=Crud">Nueva cuenta</a>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            <th style="width:180px;">Cliente</th>
            <th>Banco</th>
            <th>Codigo</th>
            <th style="width:60px;"></th>
            <th style="width:60px;"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r):
        $banco="";
        $cliente="";
        foreach($this->bancos->Listar() as $b): 
            if ($b['id']==$r['bank_id'])
                $banco=$b['name'];
        endforeach;
        foreach($this->clientes->Listar() as $c): 
            if ($c['id']==$r['client_id'])
                $cliente=$c['name'];
        endforeach; ?>
        <tr>
            <td><?php echo $cliente; ?></td>
            <td><?php echo $banco; ?></td>
            <td><?php echo $r['code']; ?></td>
            <td>
                <a href="?c=Acount&a=Crud&id=<?php echo $r['id']; ?>">Editar</a>
            </td>
            <td>
                <a onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=Acount&a=Eliminar&id=<?php echo $r['id']; ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 
