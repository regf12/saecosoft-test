<?php
class Acount
{
	private $pdo;
    
    public $id;
    public $bank_id;
    public $client_id;
    public $code;

    private $acounts;

	public function __CONSTRUCT()
	{
		$this->acounts = array();
		try
		{
			$this->pdo = Database::connect();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try 
		{
			$stmt = $this->pdo->query("SELECT * FROM acounts");
	        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
	            $this->acounts[] = $row;
	        }
	      	return $this->acounts;
	    } catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo->prepare("SELECT * FROM acounts WHERE id = ?");
			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$sql = 'DELETE FROM acounts WHERE id = :id';
	        $stmt = $this->pdo->prepare($sql);
	        $stmt->bindValue(':id', $id);
	        $stmt->execute();
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar($data)
	{
		try 
		{
			$sql = 'UPDATE acounts SET bank_id = :bank_id, client_id = :client_id, code = :code WHERE id = :id';
	        $stmt = $this->pdo->prepare($sql);
	        $stmt->bindValue(':bank_id', $data->bank_id);
	        $stmt->bindValue(':client_id', $data->client_id);
	        $stmt->bindValue(':code', $data->code);
	        $stmt->bindValue(':id', $data->id);
	        $stmt->execute();
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(Acount $data)
	{
		try 
		{
			$sql = 'INSERT INTO acounts (bank_id,client_id,code) VALUES (:bank_id,:client_id,:code)';
	        $stmt = $this->pdo->prepare($sql);
	        $stmt->bindValue(':bank_id', $data->bank_id);
	        $stmt->bindValue(':client_id', $data->client_id);
	        $stmt->bindValue(':code', $data->code);
	        $stmt->execute();
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}
