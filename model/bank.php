<?php
class Bank
{
	private $pdo;
    
    public $id;
    public $name;
    public $code;
    public $country;

    private $banks;

	public function __CONSTRUCT()
	{
		$this->banks = array();
		try
		{
			$this->pdo = Database::connect();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try 
		{
			$stmt = $this->pdo->query("SELECT * FROM banks");
	        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
	            $this->banks[] = $row;
	        }
	      	return $this->banks;
	    } catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo->prepare("SELECT * FROM banks WHERE id = ?");
			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$sql = 'DELETE FROM acounts WHERE bank_id = :id';
	        $stmt = $this->pdo->prepare($sql);
	        $stmt->bindValue(':id', $id);
	        $stmt->execute();
			$sql = 'DELETE FROM banks WHERE id = :id';
	        $stmt = $this->pdo->prepare($sql);
	        $stmt->bindValue(':id', $id);
	        $stmt->execute();
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar($data)
	{
		try 
		{
			$sql = 'UPDATE banks SET name = :name, code = :code, country = :country WHERE id = :id';
	        $stmt = $this->pdo->prepare($sql);
	        $stmt->bindValue(':name', $data->name);
	        $stmt->bindValue(':code', $data->code);
	        $stmt->bindValue(':country', $data->country);
	        $stmt->bindValue(':id', $data->id);
	        $stmt->execute();
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(Bank $data)
	{
		try 
		{
			$sql = 'INSERT INTO banks (name,code,country) VALUES (:name,:code,:country)';
	        $stmt = $this->pdo->prepare($sql);
	        $stmt->bindValue(':name', $data->name);
	        $stmt->bindValue(':code', $data->code);
	        $stmt->bindValue(':country', $data->country);
	        $stmt->execute();
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}