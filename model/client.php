<?php
class Client
{
	private $pdo;
    
    public $id;
    public $name;
    public $email;

	private $clients;

	public function __CONSTRUCT()
	{
		$this->clients = array();
		try
		{    
			$this->pdo = Database::connect();
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try 
		{
			$stmt = $this->pdo->query("SELECT * FROM clients");
	        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
	            $this->clients[] = $row;
	        }
	      	return $this->clients;
	    } catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo->prepare("SELECT * FROM clients WHERE id = ?");
			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($id)
	{
		try 
		{
			$sql = 'DELETE FROM acounts WHERE client_id = :id';
	        $stmt = $this->pdo->prepare($sql);
	        $stmt->bindValue(':id', $id);
	        $stmt->execute();
			$sql = 'DELETE FROM clients WHERE id = :id';
	        $stmt = $this->pdo->prepare($sql);
	        $stmt->bindValue(':id', $id);
	        $stmt->execute();
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar($data)
	{
		try 
		{
			$sql = 'UPDATE clients SET name = :name, email = :email WHERE id = :id';
	        $stmt = $this->pdo->prepare($sql);
	        $stmt->bindValue(':name', $data->name);
	        $stmt->bindValue(':email', $data->email);
	        $stmt->bindValue(':id', $data->id);
	        $stmt->execute();
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(Client $data)
	{
		try 
		{
			$sql = 'INSERT INTO clients (name,email) VALUES (:name,:email)';
	        $stmt = $this->pdo->prepare($sql);
	        $stmt->bindValue(':name', $data->name);
	        $stmt->bindValue(':email', $data->email);
	        $stmt->execute();
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}